﻿using System;
namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("FizzBuzz is a classic programming interview test...");
            Console.WriteLine("Classic FizzBuzz requires that the numbers 1 through 100 are printed.");
            Console.WriteLine("For multiples of 3, print Fizz instead of the number.");
            Console.WriteLine("For multiples of 5, print Buzz instead of the number.");
            Console.WriteLine("For multiples of both 3 and 5, print FizzBuzz instead of the number.");
            Console.WriteLine("This implementation allows the user to pick a maximum value, and custom Fizz and Bang values.");
            Console.WriteLine();

            Console.Write("Enter a maximum value: ");
            int maxValue = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter a Fizz value: ");
            int fizzValue = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter a Buzz value: ");
            int buzzValue = Convert.ToInt32(Console.ReadLine());

            FizzBang(maxValue, fizzValue, buzzValue);

            Console.ReadLine();
            
        }

        static void FizzBang(int max, int fizzInt, int buzzInt)
        {
            int fizzCount = 0;
            int buzzCount = 0;
            int fizzBuzzCount = 0;
            for (int i = 1; i <= max; i++)
            {

                //Console.WriteLine(i);
                if (i % fizzInt == 0 && i % buzzInt == 0)
                {
                    fizzBuzzCount++;
                    Console.WriteLine("FizzBuzz");
                }
                else if (i % fizzInt == 0)
                {
                    fizzCount++;
                    Console.WriteLine("Fizz");
                }
                else if (i % buzzInt == 0)
                {
                    buzzCount++;
                    Console.WriteLine("Buzz");
                }
                else
                {
                    Console.WriteLine(i);
                }
            }
            Console.WriteLine();
            Console.WriteLine("{0} FizzBuzzes, {1} Fizzes, and {2} Buzzes.",fizzBuzzCount,fizzCount,buzzCount);
        }
    }
}
