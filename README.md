# Fizz Buzz
*Classic FizzBuzz*: Prints the numbers 1 to 100. If the number is divisible by 3, print "Fizz". If the number is divisible by 5, print "Buzz". If the number is divisible by both 3 and 5, print "FizzBang".

*This Implementation*: Allows users to select a maximum value, as well as Fizz and Bang values. The implementation will print the numbers 1 through MAX, printing Fizz and Buzz for the specified values.